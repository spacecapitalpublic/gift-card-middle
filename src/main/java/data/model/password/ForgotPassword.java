package data.model.password;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="ForgotPasswords")
public class ForgotPassword {
	
	@Id
	private String id;
	
	private String code;
	private String account;
	private LocalDate reqeustedTime;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public LocalDate getReqeustedTime() {
		return reqeustedTime;
	}
	public void setReqeustedTime(LocalDate reqeustedTime) {
		this.reqeustedTime = reqeustedTime;
	}
}
