package data.model.offers.util;

import java.util.ArrayList;
import java.util.List;

import data.model.offers.Offer;
import data.model.offers.frontend.OfferDisplay;

public class OffersUtil {
	
//	private static final DecimalFormat format = new DecimalFormat("##.00");
	
	public static OfferDisplay convertToDisplay(Offer offer) {
		OfferDisplay display = new OfferDisplay();
		display.setOffer(offer.getCurrentOffer());
		display.setSourceName(offer.getSourceName());
		return display;
	}

	public static List<OfferDisplay> convertToList(List<Offer> offers) {
		List<OfferDisplay> displayList = new ArrayList<>();
		for(Offer offer : offers) 
			displayList.add(convertToDisplay(offer));
		return displayList;
	}
	
}
