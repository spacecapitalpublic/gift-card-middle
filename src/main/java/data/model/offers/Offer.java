package data.model.offers;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="offers")
public class Offer {
	
	@Id
	private String id;
	
	private String sourceName;
	private double currentOffer;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public double getCurrentOffer() {
		return currentOffer;
	}
	public void setCurrentOffer(double currentOffer) {
		this.currentOffer = currentOffer;
	}
}
