package data.model.accounts.frontend;

public class AccountPayementResponse {
	
	private String paymentMethod;
	
	private boolean paypalEnabled;
	private String paypalEmail;

	private boolean echeckEnabled;
	private String routing;
	private String accountNumber;
	
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public boolean isPaypalEnabled() {
		return paypalEnabled;
	}
	public void setPaypalEnabled(boolean paypalEnabled) {
		this.paypalEnabled = paypalEnabled;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public boolean isEcheckEnabled() {
		return echeckEnabled;
	}
	public void setEcheckEnabled(boolean echeckEnabled) {
		this.echeckEnabled = echeckEnabled;
	}
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
