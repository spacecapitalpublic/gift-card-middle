package data.model.accounts.frontend;

public class AccountSummary {
	
	private String nextPaymentDate;
	private String nextPaymentAmount;
	
	private String totalPayments;
	private String totalPaidOut;
	
	private String totalCardsUploaded;
	private String totalCardsInProcessing;
	private String cardsInPayout;
	private String cardsPaidOut;
	
	public String getNextPaymentDate() {
		return nextPaymentDate;
	}
	public void setNextPaymentDate(String nextPaymentDate) {
		this.nextPaymentDate = nextPaymentDate;
	}
	public String getNextPaymentAmount() {
		return nextPaymentAmount;
	}
	public void setNextPaymentAmount(String nextPaymentAmount) {
		this.nextPaymentAmount = nextPaymentAmount;
	}
	public String getTotalPayments() {
		return totalPayments;
	}
	public void setTotalPayments(String totalPayments) {
		this.totalPayments = totalPayments;
	}
	public String getTotalPaidOut() {
		return totalPaidOut;
	}
	public void setTotalPaidOut(String totalPaidOut) {
		this.totalPaidOut = totalPaidOut;
	}
	public String getTotalCardsInProcessing() {
		return totalCardsInProcessing;
	}
	public void setTotalCardsInProcessing(String totalCardsInProcessing) {
		this.totalCardsInProcessing = totalCardsInProcessing;
	}
	public String getCardsInPayout() {
		return cardsInPayout;
	}
	public void setCardsInPayout(String cardsInPayout) {
		this.cardsInPayout = cardsInPayout;
	}
	public String getCardsPaidOut() {
		return cardsPaidOut;
	}
	public void setCardsPaidOut(String cardsPaidOut) {
		this.cardsPaidOut = cardsPaidOut;
	}
	public String getTotalCardsUploaded() {
		return totalCardsUploaded;
	}
	public void setTotalCardsUploaded(String totalCardsUploaded) {
		this.totalCardsUploaded = totalCardsUploaded;
	}
}
