package data.model.accounts.frontend;

public class LoginResponseLocal {
	
	private LoginResponse response;
	private String cookieValue;

	public LoginResponse getResponse() {
		return response;
	}
	public void setResponse(LoginResponse response) {
		this.response = response;
	}
	public String getCookieValue() {
		return cookieValue;
	}
	public void setCookieValue(String cookieValue) {
		this.cookieValue = cookieValue;
	}

}
