package data.model.accounts.util;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.List;

import data.model.accounts.AccountPayements;
import data.model.accounts.frontend.AccountPayementResponse;
import data.model.accounts.frontend.AccountSummary;
import data.model.giftcards.Giftcard;
import data.model.payments.Payment;
import util.general.VersionController;

public class AccountsUtil {
	
	private static final DecimalFormat format = new DecimalFormat("##.00");
	
	public static AccountPayementResponse convertToFrontEnd(AccountPayements pay) {
		AccountPayementResponse response = new AccountPayementResponse();
		response.setPaypalEnabled(pay.isPaypalEnabled());
		response.setEcheckEnabled(pay.isEcheckEnabled());
		response.setPaymentMethod(pay.getPreferredPayementMethod());
		response.setAccountNumber(pay.getAccountNumber());
		response.setRouting(pay.getRoutingNumber());
		response.setPaypalEmail(pay.getPaypalEmail());
		return response;
	}
	
	public static AccountSummary createAccountSummary(List<Payment> payments, List<Giftcard> cards) {
		int totalCardsUploaded = cards.size();
		
		int totalCardsInProcessing = 0, cardsInPayout = 0, cardsPaidOut = 0;
		
		
		for(Giftcard card : cards) {
			if(VersionController.isCardProcessing(card.getCurrentStatus()))
				totalCardsInProcessing++;
			else if(VersionController.isCardPayingOut(card.getCurrentStatus()))
				cardsInPayout++;
			else if(VersionController.isCardPaid(card.getCurrentStatus()))
				cardsPaidOut++;
		}
		
		LocalDate nextPayment = null;
		double nextPaymentAmount = 0;
		int totalPayments = 0;
		double totalPaidOut = 0;
		
		for(Payment payment : payments) {
			if(!VersionController.isPaymentPaid(payment.getStatus())) {
				if(nextPayment == null || payment.getPaymentDate().isBefore(nextPayment)) {
					nextPayment = payment.getPaymentDate();
					nextPaymentAmount = payment.getTotalPayout();
				}
			} else {
				totalPayments++;
				totalPaidOut+=payment.getTotalPayout();
			}
		}
		
		AccountSummary summary = new AccountSummary();
		summary.setCardsInPayout(""+cardsInPayout);
		summary.setCardsPaidOut(""+cardsPaidOut);
		summary.setTotalCardsInProcessing(""+totalCardsInProcessing);
		summary.setTotalCardsUploaded(""+totalCardsUploaded);
		
		if(nextPayment != null) {
			summary.setNextPaymentAmount("$"+format.format(nextPaymentAmount));
			summary.setNextPaymentDate(nextPayment.getMonthValue()+"/"+nextPayment.getDayOfMonth());
		} else {
			summary.setNextPaymentAmount("$0.00");
			summary.setNextPaymentDate("TBD");
		}
		
		summary.setTotalPayments(""+totalPayments);
		summary.setTotalPaidOut("$"+format.format(totalPaidOut));
		
		return summary;
	}
	

}
