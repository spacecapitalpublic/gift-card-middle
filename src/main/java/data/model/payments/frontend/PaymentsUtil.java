package data.model.payments.frontend;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import data.model.payments.Payment;
import data.model.payments.PaymentLine;
import util.general.VersionController;

public class PaymentsUtil {
	
	private static final DecimalFormat format = new DecimalFormat("##.00");
	
	public static List<PaymentSearchResult> convertToResults(List<Payment> payments) {
		List<PaymentSearchResult> results = new ArrayList<>();
		
		for(Payment pay : payments) {
			PaymentSearchResult result = new PaymentSearchResult();
			result.setId(pay.getPaymentId());
			double totalStartingBalance = 0;
			for(String line : pay.getLines().keySet()) 
				totalStartingBalance+=pay.getLines().get(line).getBalance();
			double rate = pay.getTotalPayout()/totalStartingBalance * 100;
			
			result.setPayout("$"+format.format(pay.getTotalPayout())+"(%"+format.format(rate)+")");
			
			if(pay.isPaid())
				result.setStatus("PAID on " + pay.getPaidDate().getMonthValue()+"/"+pay.getPaidDate().getDayOfMonth());
			else 
				result.setStatus("PAYING on " + pay.getPaymentDate().getMonthValue()+"/"+pay.getPaymentDate().getDayOfMonth());
			
			results.add(result);
		}
		return results; 
	}
	
	public static PaymentResponse convertToResponse(Payment pay) {
		PaymentResponse response = new PaymentResponse();
		HashMap<String, PaymentLine> lines = pay.getLines();
		List<PaymentLineResponse> responseLines = new ArrayList<>();
		
		double rate = 0;
		for(String s : lines.keySet()) {
			PaymentLineResponse responseLine = new PaymentLineResponse();
			responseLine.setBalance(format.format(lines.get(s).getBalance()));
			responseLine.setCardNumber("****"+lines.get(s).getCardNumber().substring(lines.get(s).getCardNumber().length()-4, lines.get(s).getCardNumber().length()));
			responseLine.setPayout("$"+format.format(lines.get(s).getPayoutBalance())+"(%"+format.format(lines.get(s).getPayoutRate()*100)+")");
			responseLines.add(responseLine); 
			
			rate+=lines.get(s).getPayoutRate();
		}
		
		rate = rate/(double)lines.size();
		
		response.setAverageRate(format.format(rate*100.0));
		response.setLines(responseLines);
		response.setTotalCards(""+lines.size());
		response.setPayoutTotal(format.format(pay.getTotalPayout()));
		
		if(pay.getMethod().equals(VersionController.PAYMENT_NONE)) {
			response.setPaymentMethod("None");
		} else if(pay.getMethod().equals(VersionController.PAYMENT_PAYPAL)) {
			response.setPaymentMethod("PP: " + pay.getPaypalEmail());
		} else if(pay.getMethod().equals(VersionController.PAYMENT_ECHECK)) {
			response.setPaymentMethod("ACH: " + pay.getAccountNumber());
		}
		response.setPaymentId(pay.getPaymentId());

		if(pay.isPaid())
			response.setStatus("PAID on " + pay.getPaidDate().getMonthValue()+"/"+pay.getPaidDate().getDayOfMonth());
		else 
			response.setStatus("PAYING on " + pay.getPaymentDate().getMonthValue()+"/"+pay.getPaymentDate().getDayOfMonth());
		return response;
	}
	
	public static List<Payment> sortPayments(List<Payment> payments) {
		
		List<Payment> beingPaid = new ArrayList<>();
		List<Payment> paid = new ArrayList<>();
		
		for(Payment pay : payments) {
			if(pay.isPaid())
				paid.add(pay);
			else
				beingPaid.add(pay);
		}

		paid = bubbleSortPayments(paid);
		beingPaid = bubbleSortPayments(beingPaid);
		
		
		beingPaid.addAll(paid);
		return beingPaid;
	}
	
	private static List<Payment> bubbleSortPayments(List<Payment> payments) {
		for(int i = 0; i < payments.size()-1; i++) {
			for(int ii = 0; ii < payments.size()-1-i; ii++) {
				
				if(payments.get(ii).getPaymentDate().isBefore(payments.get(ii+1).getPaymentDate())) {
					Payment temp = payments.get(ii);
					payments.set(ii, payments.get(ii+1));
					payments.set(ii+1, temp);
				}
			}
		}
		return payments;
	}
	
	
}
