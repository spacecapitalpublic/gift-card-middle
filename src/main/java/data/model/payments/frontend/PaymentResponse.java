package data.model.payments.frontend;

import java.util.List;

public class PaymentResponse {
	
	private String paymentId;
	private String status;
	private String paymentMethod;

	private String payoutTotal;
	
	private String totalCards;
	private String averageRate;
	
	private List<PaymentLineResponse> lines;

	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPayoutTotal() {
		return payoutTotal;
	}
	public void setPayoutTotal(String payoutTotal) {
		this.payoutTotal = payoutTotal;
	}
	public String getTotalCards() {
		return totalCards;
	}
	public void setTotalCards(String totalCards) {
		this.totalCards = totalCards;
	}
	public String getAverageRate() {
		return averageRate;
	}
	public void setAverageRate(String averageRate) {
		this.averageRate = averageRate;
	}
	public List<PaymentLineResponse> getLines() {
		return lines;
	}
	public void setLines(List<PaymentLineResponse> lines) {
		this.lines = lines;
	}
	
	

}
