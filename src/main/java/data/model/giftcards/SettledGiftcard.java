package data.model.giftcards;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="SettledGiftcards")
public class SettledGiftcard {
	
	@Id
	private String id;
	
	private String email; 
	
	private String number;
	private String pin;
	
	private LocalDate uploadDate;
	private double proclaimedBalance;
	
	private double payoutRate;
	private double payoutBalance;
	
	private LocalDate verifiedDate;
	private double verifiedBalance;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public LocalDate getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(LocalDate uploadDate) {
		this.uploadDate = uploadDate;
	}
	public double getProclaimedBalance() {
		return proclaimedBalance;
	}
	public void setProclaimedBalance(double proclaimedBalance) {
		this.proclaimedBalance = proclaimedBalance;
	}
	public LocalDate getVerifiedDate() {
		return verifiedDate;
	}
	public void setVerifiedDate(LocalDate verifiedDate) {
		this.verifiedDate = verifiedDate;
	}
	public double getVerifiedBalance() {
		return verifiedBalance;
	}
	public void setVerifiedBalance(double verifiedBalance) {
		this.verifiedBalance = verifiedBalance;
	}
	public double getPayoutRate() {
		return payoutRate;
	}
	public void setPayoutRate(double payoutRate) {
		this.payoutRate = payoutRate;
	}
	public double getPayoutBalance() {
		return payoutBalance;
	}
	public void setPayoutBalance(double payoutBalance) {
		this.payoutBalance = payoutBalance;
	}
}
