package data.model.giftcards;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Giftcards")
public class Giftcard {
	
	@Id
	private String id;
	private String sourceName;
	private String email; 
	
	private String number;
	private String pin;
	
	private LocalDate uploadDate;
	private double proclaimedBalance;
	
	private LocalDate verifiedDate;
	private double verifiedBalance;
	
	private LocalDate paymentDate;
	private int paymentDateId;
	
	private LocalDate lastUpdate;

	private double payoutRate;
	private double payoutBalance;
	
	private String currentStatus;
	private String failedVerifyReason;
	private String failedSpendingReason;
	
	private String errorType;
	
	private boolean canReverify;
	private double totalSpent;
	
	private int resubmitCount;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public LocalDate getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(LocalDate uploadDate) {
		this.uploadDate = uploadDate;
	}
	public double getProclaimedBalance() {
		return proclaimedBalance;
	}
	public void setProclaimedBalance(double proclaimedBalance) {
		this.proclaimedBalance = proclaimedBalance;
	}
	public LocalDate getVerifiedDate() {
		return verifiedDate;
	}
	public void setVerifiedDate(LocalDate verifiedDate) {
		this.verifiedDate = verifiedDate;
	}
	public double getVerifiedBalance() {
		return verifiedBalance;
	}
	public void setVerifiedBalance(double verifiedBalance) {
		this.verifiedBalance = verifiedBalance;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getFailedVerifyReason() {
		return failedVerifyReason;
	}
	public void setFailedVerifyReason(String failedVerifyReason) {
		this.failedVerifyReason = failedVerifyReason;
	}
	public String getFailedSpendingReason() {
		return failedSpendingReason;
	}
	public void setFailedSpendingReason(String failedSpendingReason) {
		this.failedSpendingReason = failedSpendingReason;
	}
	public double getPayoutRate() {
		return payoutRate;
	}
	public void setPayoutRate(double payoutRate) {
		this.payoutRate = payoutRate;
	}
	public boolean isCanReverify() {
		return canReverify;
	}
	public void setCanReverify(boolean canReverify) {
		this.canReverify = canReverify;
	}
	public LocalDate getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(LocalDate lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public double getTotalSpent() {
		return totalSpent;
	}
	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	public int getPaymentDateId() {
		return paymentDateId;
	}
	public void setPaymentDateId(int paymentDateId) {
		this.paymentDateId = paymentDateId;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public double getPayoutBalance() {
		return payoutBalance;
	}
	public void setPayoutBalance(double payoutBalance) {
		this.payoutBalance = payoutBalance;
	}
	public int getResubmitCount() {
		return resubmitCount;
	}
	public void setResubmitCount(int resubmitCount) {
		this.resubmitCount = resubmitCount;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
}
