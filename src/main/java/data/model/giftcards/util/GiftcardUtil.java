package data.model.giftcards.util;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import data.model.giftcards.Giftcard;
import data.model.giftcards.SettledGiftcard;
import data.model.giftcards.frontend.GiftcardDisplay;
import data.model.giftcards.frontend.GiftcardResult;
import util.general.VersionController;

public class GiftcardUtil {
	
	private static final DecimalFormat format = new DecimalFormat("##.00");
	
	public static Giftcard createNewGiftcard(String email, String cardNumber, String cardPin, double proclaimedBalance, String sourceName, double currentOffer) {
		Giftcard card = new Giftcard();
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING);
		card.setEmail(email);
		card.setFailedSpendingReason("");
		card.setFailedVerifyReason("");
		card.setNumber(cardNumber);
		card.setPin(cardPin);
		card.setUploadDate(LocalDate.now());
		card.setProclaimedBalance(proclaimedBalance);
		card.setPayoutRate(currentOffer);
		card.setPayoutBalance(currentOffer * proclaimedBalance);
		return card;
	}
	
	public static List<GiftcardResult> convertToResults(List<Giftcard> cards) {
		List<GiftcardResult> list = new ArrayList<>();
		for(Giftcard card : cards) {
			if(card == null)
				continue;
			GiftcardResult result = new GiftcardResult();
			result.setCardNumber("****"+card.getNumber().substring(card.getNumber().length()-4, card.getNumber().length()));
			result.setStatus(card.getCurrentStatus());
			result.setCardNumberFull(card.getNumber());
			result.setPayoutBalance(format.format(card.getPayoutBalance()));
			
			if(card.getVerifiedBalance() == 0)
				result.setBalance(format.format(card.getProclaimedBalance()));
			else
				result.setBalance(format.format(card.getVerifiedBalance()));
			
			list.add(result);
		}
		return list;
	}
	
	public static List<GiftcardResult> convertToResultsSettled(List<SettledGiftcard> cards) {
		List<GiftcardResult> list = new ArrayList<>();
		for(SettledGiftcard card : cards) {
			if(card == null)
				continue;
			GiftcardResult result = new GiftcardResult();
			result.setCardNumber("****"+card.getNumber().substring(card.getNumber().length()-4, card.getNumber().length()));
			result.setCardNumberFull(card.getNumber());
			result.setStatus(VersionController.GIFTCARD_STATUS_SETTLED);
			result.setPayoutBalance(format.format(card.getPayoutBalance()));
			
			if(card.getVerifiedBalance() == 0)
				result.setBalance(format.format(card.getProclaimedBalance()));
			else
				result.setBalance(format.format(card.getVerifiedBalance()));
			
			list.add(result);
		}
		return list;
	}

	public static GiftcardDisplay convertToDisplay(Giftcard card) {
		GiftcardDisplay display = new GiftcardDisplay();
		display.setCardNumber(card.getNumber());
		display.setPayoutBalance(card.getPayoutBalance());
		display.setPayoutRate("%"+format.format(card.getPayoutRate()*100));
		display.setProclaimedBalance(card.getProclaimedBalance());
		display.setStatus(card.getCurrentStatus());
		display.setVerifiedBalance(card.getVerifiedBalance());
		
		//set the dates
		display.setUploadDate(convertToSimpleString(card.getUploadDate()));
		
		if(card.getLastUpdate() == null)
			display.setLastUpdate(convertToSimpleString(card.getUploadDate()));
		else
			display.setLastUpdate(convertToSimpleString(card.getLastUpdate()));
		
		if(card.getPaymentDate() != null)
			display.setPayoutDate(convertToSimpleString(card.getPaymentDate()));
		else
			display.setPayoutDate("TBD");
		
		if(card.getCurrentStatus().equals(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM))
			display.setErrorMessage(card.getFailedVerifyReason());
		else if(card.getCurrentStatus().equals(VersionController.GIFTCARD_STATUS_SPENDING_PROMBLEM))
			display.setErrorMessage(card.getFailedSpendingReason());
		else
			display.setErrorMessage("");
		if(!display.getErrorMessage().equals("")) {
			display.setError(true);
			
			if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_BALANCE)) {
				display.setResubmitMessage("Resubmit card at $" + card.getVerifiedBalance() + " balance");
			} else if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_DETAILS)) {
				display.setResubmitMessage("Retry Verification");
			} else if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_DUPLICATE)) {
				display.setResubmitMessage("Delete Duplicate card");
			} else if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_OTHER)) {
				display.setResubmitMessage("Retry Verification");
			}
		}
		return display;
	}
	
	public static GiftcardDisplay convertSettledToDisplay(SettledGiftcard card) {
		GiftcardDisplay display = new GiftcardDisplay();
		display.setCardNumber(card.getNumber());
		display.setPayoutBalance(card.getPayoutBalance());
		display.setPayoutRate("%"+format.format(card.getPayoutRate()*100));
		display.setProclaimedBalance(card.getProclaimedBalance());
		display.setStatus(VersionController.GIFTCARD_STATUS_SETTLED);
		display.setVerifiedBalance(card.getVerifiedBalance());
		display.setErrorMessage("");
		return display;
	}
	
	private static String convertToSimpleString(LocalDate date) {
		return date.getMonthValue()+"/"+date.getDayOfMonth()+"/"+date.getYear();
	}
	
	public static List<Giftcard> sortCards(List<Giftcard> cards) {
		HashMap<String, List<Giftcard>> cardMap = new HashMap<>();
		
		for(Giftcard card : cards) {
			if(!cardMap.containsKey(card.getCurrentStatus()))
				cardMap.put(card.getCurrentStatus(), new ArrayList<Giftcard>());
			cardMap.get(card.getCurrentStatus()).add(card); 
		}
		
		for(String s : cardMap.keySet()) 
			cardMap.put(s, bubbleSortCards(cardMap.get(s)));

		String sortArray[] = {VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM, VersionController.GIFTCARD_STATUS_SPENDING_PROMBLEM, VersionController.GIFTCARD_STATUS_VERIFYING, 
				VersionController.GIFTCARD_STATUS_VERIFIED, VersionController.GIFTCARD_STATUS_SPENDING, VersionController.GIFTCARD_STATUS_PAYING, VersionController.GIFTCARD_STATUS_PAYING_SCHEDULED,
				VersionController.GIFTCARD_STATUS_PAID, VersionController.GIFTCARD_STATUS_SETTLED};
		
		List<Giftcard> sorted = new ArrayList<>();
		for(String s : sortArray) {
			if(cardMap.containsKey(s))
				sorted.addAll(cardMap.get(s));
		}
		
		return sorted;
	}
	
	private static List<Giftcard> bubbleSortCards(List<Giftcard> cards) {
		for(int i = 0; i < cards.size()-1; i++) {
			for(int ii = 0; ii < cards.size()-1-i; ii++) {
				if(cards.get(ii).getUploadDate().isBefore(cards.get(ii+1).getUploadDate())) {
					Giftcard temp = cards.get(ii);
					cards.set(ii, cards.get(ii+1));
					cards.set(ii+1, temp);
				}
			}
		}
		return cards;
	}
	
}
