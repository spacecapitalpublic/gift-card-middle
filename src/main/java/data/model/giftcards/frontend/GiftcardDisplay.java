package data.model.giftcards.frontend;

public class GiftcardDisplay {
	
	private String cardNumber;
	private String status; 
	
	private String uploadDate;
	private String lastUpdate;
	private String payoutDate;
	
	private double proclaimedBalance;
	private double verifiedBalance;
	
	private String payoutRate;
	private double payoutBalance;
	
	private boolean error;
	private boolean canDelete;
	private String errorMessage;
	private String resubmitMessage;
	
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getPayoutDate() {
		return payoutDate;
	}
	public void setPayoutDate(String payoutDate) {
		this.payoutDate = payoutDate;
	}
	public double getProclaimedBalance() {
		return proclaimedBalance;
	}
	public void setProclaimedBalance(double proclaimedBalance) {
		this.proclaimedBalance = proclaimedBalance;
	}
	public double getVerifiedBalance() {
		return verifiedBalance;
	}
	public void setVerifiedBalance(double verifiedBalance) {
		this.verifiedBalance = verifiedBalance;
	}
	public String getPayoutRate() {
		return payoutRate;
	}
	public void setPayoutRate(String payoutRate) {
		this.payoutRate = payoutRate;
	}
	public double getPayoutBalance() {
		return payoutBalance;
	}
	public void setPayoutBalance(double payoutBalance) {
		this.payoutBalance = payoutBalance;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getResubmitMessage() {
		return resubmitMessage;
	}
	public void setResubmitMessage(String resubmitMessage) {
		this.resubmitMessage = resubmitMessage;
	}
	public boolean isCanDelete() {
		return canDelete;
	}
	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}
}
