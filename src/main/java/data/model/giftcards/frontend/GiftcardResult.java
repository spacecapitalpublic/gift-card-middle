package data.model.giftcards.frontend;

public class GiftcardResult {
	
	private String cardNumberFull;
	private String cardNumber;
	private String status; 
	
	private String payoutBalance;
	private String balance;

	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPayoutBalance() {
		return payoutBalance;
	}
	public void setPayoutBalance(String payoutBalance) {
		this.payoutBalance = payoutBalance;
	}
	public String getCardNumberFull() {
		return cardNumberFull;
	}
	public void setCardNumberFull(String cardNumberFull) {
		this.cardNumberFull = cardNumberFull;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
}
