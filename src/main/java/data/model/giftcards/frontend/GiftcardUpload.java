package data.model.giftcards.frontend;

public class GiftcardUpload {
	
	private String cardNumber;
	private String cardPin;
	private String proclaimedBalance;
	private String sourceName;
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardPin() {
		return cardPin;
	}
	public void setCardPin(String cardPin) {
		this.cardPin = cardPin;
	}
	public String getProclaimedBalance() {
		return proclaimedBalance;
	}
	public void setProclaimedBalance(String proclaimedBalance) {
		this.proclaimedBalance = proclaimedBalance;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
}
