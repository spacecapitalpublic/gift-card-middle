package repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.logs.LogData;

public interface LogDataRepo extends MongoRepository<LogData, String>{
	
	
}
