package repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.giftcards.Giftcard;

public interface GiftcardRepo extends MongoRepository<Giftcard, String>{
	public Giftcard findByNumber(String number); 
	public Giftcard findByEmailAndNumber(String email, String number);
	public List<Giftcard> findByCurrentStatusAndEmail(String currentStatus, String email);
	public List<Giftcard> findByEmail(String email);
}
