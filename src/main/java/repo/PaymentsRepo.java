package repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.payments.Payment;

public interface PaymentsRepo extends MongoRepository<Payment, String>{
	
	public List<Payment> findByEmail(String email);
	public Payment findByEmailAndPaymentDateId(String email, int paymentDateId);
	public Payment findByPaymentId(String paymentId); 
	
}
