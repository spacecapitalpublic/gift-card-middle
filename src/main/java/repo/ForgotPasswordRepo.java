package repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.password.ForgotPassword;

public interface ForgotPasswordRepo extends MongoRepository<ForgotPassword, String>{

	public ForgotPassword findByCode(String code);
	public ForgotPassword findByAccount(String account);
	
}
