package repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.accounts.AccountPayements;

public interface AccountPaymentRepo extends MongoRepository<AccountPayements, String>{
	public AccountPayements findByEmail(String email);
}
