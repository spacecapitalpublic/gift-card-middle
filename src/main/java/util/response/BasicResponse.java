package util.response;

public class BasicResponse {
	
	private String response;

	public BasicResponse(String response) {
		this.response = response;
	}
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	

}
