package util.general;

import java.util.Random;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import security.util.TokenManager;

public class GeneralUtil {
	
	private static final Random random = new Random();
	
	public static boolean isEmailValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                "[a-zA-Z0-9_+&*-]+)*@" + 
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                "A-Z]{2,7}$"; 
                  
		Pattern pat = Pattern.compile(emailRegex); 
		if (email == null) 
		return false; 
		return pat.matcher(email).matches(); 
		
	}
	
	public static String generateRandomString(int length) {
		String set = "abcdefghijklmnopqrstuvxyz0123456789";
		String dope = "";
		for(int i = 0; i < length; i++) {
			dope+=set.charAt(random.nextInt(set.length()));
		}
		return dope;
	}
	
	public static String getTokenFromAuthHeader(String authHeader) {
		if(!authHeader.startsWith("Bearer "))
			return "";
		if(authHeader.split("Bearer ").length < 1)
			return "";
		try {
			return authHeader.split("Bearer ")[1];
		} catch(Exception e) {
			return "";
		}
	}
	
	public static String getEmailFromTokenFromHeader(String authHeader) {
		String token = getTokenFromAuthHeader(authHeader);
		if(token.equals(""))
			return "";
		else
			return TokenManager.findEmailByToken(token);
	}
	
	public static String getCookie(HttpServletRequest request, String cookieName) {
		String header = request.getHeader("Cookie");
		if(header == null)
			return "";
		String[] cookiesRaw = header.split(";");
		
		for(String s : cookiesRaw) {
			String name = s.split("=")[0];
			String value = s.split("=")[1];
			if(name.equals(cookieName))
				return value.trim();
		}
		return "";
	}
	
	
}
