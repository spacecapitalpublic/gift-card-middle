package security.util;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TokenManager {
	
	private static final long hour = 1000*60*60;

	private static ConcurrentHashMap<String, Token> emailTokenMap = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, Token> tokenMap = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, String> tokenEmailMap = new ConcurrentHashMap<>();
	
	public static String login(String email) {
		Token token = generateToken();
		if(emailTokenMap.containsKey(email)) {
			Token oldToken = emailTokenMap.get(email);
			tokenMap.remove(oldToken.getToken());
			tokenEmailMap.remove(oldToken.getToken());
		}
		
		emailTokenMap.put(email, token);
		tokenMap.put(token.getToken(), token);
		tokenEmailMap.put(token.getToken(), email); 
		
		return token.getToken();
	}
	
	private static Token generateToken() {
		String tokenString = UUID.randomUUID().toString();
		boolean alreadyExists = true;
		while(alreadyExists) 
			if(!tokenMap.containsKey(tokenString))
				alreadyExists = false;
			else
				tokenString = UUID.randomUUID().toString();
		Token token = new Token();
		token.setToken(tokenString);
		token.setStartTime(System.currentTimeMillis());
		token.setEndTime(System.currentTimeMillis()+(hour*4));
		return token;
	}
	
	public static boolean isTokenValid(String tokenString) {
		Token token = tokenMap.get(tokenString); 
		if(token == null)
			return false;
		if(token.getEndTime() < System.currentTimeMillis()) {
			//delete the token from memory
			String email = tokenEmailMap.get(tokenString);
			emailTokenMap.remove(email);
			tokenMap.remove(tokenString);
			tokenEmailMap.remove(tokenString);
			return false;
		}
		return true;
	}
	
	public static String findEmailByToken(String tokenString) {
		if(tokenEmailMap.containsKey(tokenString))
			return tokenEmailMap.get(tokenString);
		else
			return "";
	}
	
}
