package security.util;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CookieManager {
	
	private static final long hour = 1000*60*60;

	private static ConcurrentHashMap<String, Token> emailCookieMap = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, Token> cookieMap = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, String> cookieEmailMap = new ConcurrentHashMap<>();
	
	public static String login(String email) {
		Token token = generateToken();
		if(emailCookieMap.containsKey(email)) {
			Token oldToken = emailCookieMap.get(email);
			cookieMap.remove(oldToken.getToken());
			cookieEmailMap.remove(oldToken.getToken());
		}
		
		emailCookieMap.put(email, token);
		cookieMap.put(token.getToken(), token);
		cookieEmailMap.put(token.getToken(), email); 
		
		return token.getToken();
	}
	
	private static Token generateToken() {
		String tokenString = UUID.randomUUID().toString();
		boolean alreadyExists = true;
		while(alreadyExists) 
			if(!cookieMap.containsKey(tokenString))
				alreadyExists = false;
			else
				tokenString = UUID.randomUUID().toString();
		Token token = new Token();
		token.setToken(tokenString);
		token.setStartTime(System.currentTimeMillis());
		token.setEndTime(System.currentTimeMillis()+(hour*4));
		return token;
	}
	
	public static boolean isTokenValid(String tokenString) {
		Token token = cookieMap.get(tokenString); 
		if(token == null)
			return false;
		if(token.getEndTime() < System.currentTimeMillis()) {
			//delete the token from memory
			String email = cookieEmailMap.get(tokenString);
			emailCookieMap.remove(email);
			cookieMap.remove(tokenString);
			cookieEmailMap.remove(tokenString);
			return false;
		}
		return true;
	}
	
	public static String findEmailByToken(String tokenString) {
		if(cookieEmailMap.containsKey(tokenString))
			return cookieEmailMap.get(tokenString);
		else
			return "";
	}

}
