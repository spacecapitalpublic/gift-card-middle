package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import data.model.accounts.Account;
import repo.AccountsRepo;
import security.util.TokenManager;

@Service("customerService")	
public class CustomCustomerService implements CustomerService {
	
	@Autowired
	private AccountsRepo userRepo;

	@Override
	public String login(String email, String password) {
		//find the user
		Account user = userRepo.findByEmail(email);
		if(user == null)
			return "";
		if(!user.getPassword().equals(password))
			return "";

		String token = TokenManager.login(email);
		return token;
	}

	@Override
	public User findByToken(String token) {
		String email = TokenManager.findEmailByToken(token); 
		
		if(email.length() > 0) {
			User user= new User("user", "password", true, true, true, true,
                    AuthorityUtils.createAuthorityList("USER"));
			return user;
		}
		return null;
	}

	@Override
	public Account findByUsername(String email) {
		Account user = userRepo.findByEmail(email);
		if(user == null)
			return null;
		return user;
	}

}
