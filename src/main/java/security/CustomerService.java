package security;

import org.springframework.security.core.userdetails.User;

import data.model.accounts.Account;

public interface CustomerService {
	String login(String username, String password);
    User findByToken(String token);
    Account findByUsername(String username);
}
