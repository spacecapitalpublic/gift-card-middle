package main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import repo.AccountsRepo;
import rest.controller.AccountController;
import rest.service.AccountService;
import security.AuthenticationFilter;
import security.config.SpringSecurityConfig;
import service.email.EmailSenderService;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = {AccountsRepo.class})
@ComponentScan(
		basePackageClasses = {EmailSenderService.class, AccountService.class, AccountController.class, SpringSecurityConfig.class, AuthenticationFilter.class})
@EnableAutoConfiguration
public class MiddleService {
	
	public static void main(String[] args) {
		SpringApplication.run(MiddleService.class, args);
	}
	
	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}
