package rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.offers.Offer;
import data.model.offers.frontend.OfferDisplay;
import data.model.offers.util.OffersUtil;
import repo.OffersRepo;
import rest.service.error.ServiceError;
import util.general.VersionController;

@Service
public class OfferService {
	
	@Autowired
	private OffersRepo repo;
	
	public List<OfferDisplay> getAllOffers() {
		List<Offer> offers = repo.findAll();
		return OffersUtil.convertToList(offers);
	}
	
	public OfferDisplay getOffer(String sourceName) throws ServiceError {
		if(sourceName.equals("HD"))
			return OffersUtil.convertToDisplay(repo.findBySourceName(VersionController.SOURCE_HOMEDEPOT));
		throw new ServiceError("Invalid source");
	}
}
