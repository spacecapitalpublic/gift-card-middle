package rest.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import data.model.accounts.Account;
import data.model.accounts.AccountPayements;
import data.model.accounts.frontend.AccountPayementResponse;
import data.model.accounts.frontend.AccountSummary;
import data.model.accounts.frontend.ChangePasswordRequest;
import data.model.accounts.frontend.ForgotChangePassword;
import data.model.accounts.frontend.ForgotPasswordRequest;
import data.model.accounts.frontend.LoginResponse;
import data.model.accounts.frontend.LoginResponseLocal;
import data.model.accounts.frontend.PaymentMethodsUpdate;
import data.model.accounts.frontend.SignInRequest;
import data.model.accounts.frontend.SignUpRequest;
import data.model.accounts.util.AccountsUtil;
import data.model.giftcards.Giftcard;
import data.model.logs.LogData;
import data.model.password.ForgotPassword;
import data.model.payments.Payment;
import repo.AccountPaymentRepo;
import repo.AccountsRepo;
import repo.ForgotPasswordRepo;
import repo.GiftcardRepo;
import repo.LogDataRepo;
import repo.PaymentsRepo;
import rest.service.error.ServiceError;
import security.util.CookieManager;
import security.util.TokenManager;
import service.email.EmailSenderService;
import util.general.GeneralUtil;
import util.general.VersionController;

@Service
public class AccountService {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private AccountsRepo accountRepo;
	
	@Autowired
	private AccountPaymentRepo paymentRepo;
	
	@Autowired
	private GiftcardRepo giftcardRepo;
	
	@Autowired
	private PaymentsRepo paymentsRepo;
	
	@Autowired
	private ForgotPasswordRepo forgotRepo;
	
	@Autowired
	private LogDataRepo logRepo;
	
	@Autowired
	private EmailSenderService emailService;
	
	public String signUp(SignUpRequest request) throws ServiceError {
		String email = request.getEmail().toLowerCase().trim();
		String name = request.getName();
		String password = request.getPassword();
		
		if(email == null || name == null || password == null ||
				email.length() == 0 || name.length() == 0 || password.length() == 0)
			throw new ServiceError("Need all fields");
		
		if(accountRepo.findByEmail(email) != null)
			throw new ServiceError("Email already exists");
		
		if(!GeneralUtil.isEmailValid(email))
			throw new ServiceError("Use a valid email"); 
		
		if(password.length() < 10)
			throw new ServiceError("Password is too short"); 
		
		Account account = new Account();
		account.setEmail(email);
		account.setName(name);
		account.setPassword(encoder.encode(password));
		account.setVerified(true);
		account.setVerifyCode(this.sendVerifyEmail(email));
		
		accountRepo.save(account);
		return "good";
	}
	
	private String sendVerifyEmail(String email) {
		String randomString = GeneralUtil.generateRandomString(25);
		String body = "Click this link to verify the account: https://bestratecards.com/#/authentication/login?verifycode="+randomString;
		emailService.sendEmail(email, "Verfiy your account for Best Rate Cards", body);
		return randomString;
	}
	
	public LoginResponseLocal signIn(SignInRequest request, String ipAddress) throws ServiceError {
		String email = request.getEmail().toLowerCase().trim();
		String password = request.getPassword();
		
		Account account = accountRepo.findByEmail(email);
		if(account == null)
			throw new ServiceError("Invalid username/password");

		if(!encoder.matches(password, account.getPassword()))
			throw new ServiceError("Invalid username/password");

		if(!account.isVerified())
			throw new ServiceError("Account needs to be verified");
		
		String token = TokenManager.login(email);
		String name = account.getName();
		
		String cookieToken = CookieManager.login(email); 
		
		LoginResponse response = new LoginResponse();
		response.setName(name);
		response.setToken(token);
		
		LoginResponseLocal responseLocal = new LoginResponseLocal();
		responseLocal.setCookieValue(cookieToken);
		responseLocal.setResponse(response);
		
		
		LogData data = new LogData();
		data.setAccount(account.getEmail());
		data.setNotes("Login");
		data.setType("Login");
		data.setTime(LocalDate.now());
		data.setIpAddress(ipAddress);
		logRepo.save(data);
		
		
		return responseLocal;
	}
	
	public String verifyAccount(String verifyCode) throws ServiceError{
		Account account = accountRepo.findByVerifyCode(verifyCode);
		if(account == null)
			throw new ServiceError("No account with that verify code");
	
		account.setVerified(true);
		account.setVerifyCode("");
		accountRepo.save(account);
		
		return "good";
	}
	
	public AccountPayementResponse getAccountPayements(String authHeader) throws ServiceError {
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 
		AccountPayements pay = paymentRepo.findByEmail(email);
		if(pay == null) {
			AccountPayements payements = new AccountPayements();
			payements.setEmail(email);
			payements.setEcheckEnabled(false);
			payements.setPaypalEnabled(false);
			payements.setPreferredPayementMethod(VersionController.PAYMENT_NONE);
			payements.setAccountNumber("");
			payements.setPaypalEmail("");
			payements.setRoutingNumber("");
			paymentRepo.save(payements); 
			return AccountsUtil.convertToFrontEnd(payements);
		}
		return AccountsUtil.convertToFrontEnd(pay);
	}
	
	public String updatePaymentMethods(String authHeader, PaymentMethodsUpdate request) throws ServiceError {
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 
		AccountPayements pay = paymentRepo.findByEmail(email);
		if(pay == null)
			pay = new AccountPayements();
		
		
		//check for paypal info
		String paypalEmail = request.getPaypalEmail().trim();
		if(paypalEmail.length() != 0) {
			boolean isValid = GeneralUtil.isEmailValid(paypalEmail); 
			if(!isValid) {
				pay.setPaypalEnabled(false);
				pay.setPaypalEmail("");
				throw new ServiceError("Invalid paypal email"); 
			} else {
				pay.setPaypalEnabled(true);
				pay.setPaypalEmail(paypalEmail);
			}
		} else {
			pay.setPaypalEmail("");
			pay.setPaypalEnabled(false);
		}

		//check for echeck info 
		String routing = request.getRouting().trim();
		String accountNumber = request.getAccountNumber().trim();
		if(routing.length() == 0 || accountNumber.length() == 0) {
			pay.setEcheckEnabled(false);
			pay.setAccountNumber("");
			pay.setRoutingNumber("");
		} else {
			if(routing.length() != 9)
				throw new ServiceError("Invalid routing number"); 
			pay.setRoutingNumber(routing);
			pay.setAccountNumber(accountNumber);
			pay.setEcheckEnabled(true);
		}
		
		String method = request.getPaymentMethod();
		if(method.equals(VersionController.PAYMENT_ECHECK) || method.equals(VersionController.PAYMENT_PAYPAL) || method.equals(VersionController.PAYMENT_NONE)) {
			pay.setPreferredPayementMethod(method);
			
			if(method.equals(VersionController.PAYMENT_ECHECK)) 
				if(!pay.isEcheckEnabled())
					throw new ServiceError("Need ACH data for ACH method");
			if(method.equals(VersionController.PAYMENT_PAYPAL))
				if(!pay.isPaypalEnabled())
					throw new ServiceError("Need Paypal email for Paypal method");
		} else
			throw new ServiceError("Invalid payment mehtod selection");
		paymentRepo.save(pay);
		return "Payment methods updated"; 
	}
	
	public String changePassword(String authHeader, ChangePasswordRequest request) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 
		
		Account account = accountRepo.findByEmail(email);
		if(!encoder.matches(request.getOldPassword(), account.getPassword()))
			throw new ServiceError("Invalid old password"); 
		
		if(request.getNewPassword().length() < 10)
			throw new ServiceError("New password too weak"); 
		
		account.setPassword(encoder.encode(request.getNewPassword()));
		return "Password changed";
	}
	
	public AccountSummary getAccountSummary(String authHeader) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 
		
		List<Payment> payments = paymentsRepo.findByEmail(email);
		List<Giftcard> cards = giftcardRepo.findByEmail(email);
		
		return AccountsUtil.createAccountSummary(payments, cards);
	}
	
	public String changePassword(ForgotChangePassword request) throws ServiceError{
		
		String code = request.getCode();
		String password = request.getPassword();
		
		if(code == null || password == null)
			throw new ServiceError("Please include a password");
		
		ForgotPassword forgot = forgotRepo.findByCode(code);
		if(forgot == null)
			throw new ServiceError("Invalid forgot password code, make another forgot password request");
		
		Account account = accountRepo.findByEmail(forgot.getAccount());
		if(account == null)
			throw new ServiceError("Invalid forgot password code, make another forgot password request");
		
		if(forgot.getReqeustedTime().plusDays(1).isBefore(LocalDate.now())) {
			forgotRepo.delete(forgot);
			throw new ServiceError("Forgot password code expired, make another forgot password request");
		}
		
		if(password.length() < 10)
			return "Password is too short";
		
		account.setPassword(encoder.encode(password));
		
		accountRepo.save(account);
		forgotRepo.delete(forgot);
		
		return "Password changed, login";
	}
	
	public String forgotPassword(ForgotPasswordRequest request) throws ServiceError {
		
		String email = request.getEmail().toLowerCase();
		Account account = accountRepo.findByEmail(email);
		
		if(account == null)
			return "Email sent to the account";
		
		if(!account.isVerified())
			return "Account not verified";

		ForgotPassword forgot = forgotRepo.findByAccount(email);
		if(forgot == null) {
			forgot = this.generateForgotPassword(forgot, email);
		} else {
			if(forgot.getReqeustedTime().plusDays(1).isBefore(LocalDate.now()))
				forgot = this.generateForgotPassword(forgot, email);
			else 
				forgot.setReqeustedTime(LocalDate.now());
		}

		emailService.sendEmail(account.getEmail(), "Forgot password instructions", "Click this link to reset your password: bestratecards.com/#/authentication/changepassword?code="+forgot.getCode());
		forgotRepo.save(forgot);
		
		return "Email sent to the account";
	}
	
	private ForgotPassword generateForgotPassword(ForgotPassword forgot, String email) {
		if(forgot == null)
			forgot = new ForgotPassword();
		String code = GeneralUtil.generateRandomString(25);
		forgot.setCode(code);
		forgot.setAccount(email);
		forgot.setReqeustedTime(LocalDate.now());
		return forgot;
	}
	
	public boolean isTokenGood(String authHeader, String authCookie) throws ServiceError {
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		if(!CookieManager.isTokenValid(authCookie))
			throw new ServiceError("Invalid token");
		return true;
	}
	
}
