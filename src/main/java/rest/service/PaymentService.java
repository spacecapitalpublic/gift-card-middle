package rest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.payments.Payment;
import data.model.payments.frontend.PaymentResponse;
import data.model.payments.frontend.PaymentSearchResult;
import data.model.payments.frontend.PaymentsUtil;
import repo.PaymentsRepo;
import rest.service.error.ServiceError;
import util.general.GeneralUtil;

@Service
public class PaymentService {
	
	@Autowired
	private PaymentsRepo paymentsRepo;
	
	public List<PaymentSearchResult> getResults(String authHeader) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 
		
		List<Payment> results = paymentsRepo.findByEmail(email);
		results = PaymentsUtil.sortPayments(results);
		return PaymentsUtil.convertToResults(results);
	}
	
	public PaymentResponse getPayment(String authHeader, String id) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.length() == 0)
			throw new ServiceError("Cant find account"); 

		Payment pay = paymentsRepo.findByPaymentId(id); 
		
		if(pay == null || !pay.getEmail().equals(email))
			throw new ServiceError("No payment with that id");
		
		return PaymentsUtil.convertToResponse(pay);
	}

}
