package rest.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.giftcards.Giftcard;
import data.model.giftcards.SettledGiftcard;
import data.model.giftcards.frontend.GiftcardDisplay;
import data.model.giftcards.frontend.GiftcardResult;
import data.model.giftcards.frontend.GiftcardUpload;
import data.model.giftcards.util.GiftcardUtil;
import data.model.offers.Offer;
import repo.GiftcardRepo;
import repo.OffersRepo;
import repo.SettledGiftcardRepo;
import rest.service.error.ServiceError;
import util.general.GeneralUtil;
import util.general.VersionController;

@Service
public class GiftcardService {
	
	@Autowired
	private GiftcardRepo giftcardRepo;
	
	@Autowired
	private OffersRepo offerRepo;
	
	@Autowired
	private SettledGiftcardRepo settledGiftcardRepo;
	
	public String uploadGiftcard(String authHeader, GiftcardUpload upload) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		
		String sourceName = upload.getSourceName();
		if(!sourceName.equals(VersionController.SOURCE_HOMEDEPOT)) 
			throw new ServiceError("Invalid source");
		
		String cardNumber = upload.getCardNumber().trim();
		String cardPin = upload.getCardPin().trim();

		double proclaimedBalance = 0;
		try {
			String cleaned = upload.getProclaimedBalance().replaceAll(Pattern.quote(","), "").trim();
			proclaimedBalance = Double.parseDouble(cleaned); 
		} catch(Exception e) {
			throw new ServiceError("Invalid card balance");
		}
		
		if(proclaimedBalance > 1000 || proclaimedBalance < 5) 
			throw new ServiceError("Card balance is too low or high"); 
		
		if(cardNumber.length() != 23)
			throw new ServiceError("Invalid card number");
		if(cardPin.length() != 4)
			throw new ServiceError("Invalid card pin"); 
		
		if(giftcardRepo.findByNumber(cardNumber)!= null || settledGiftcardRepo.findByNumber(cardNumber) != null)
			throw new ServiceError("We have a card with this card number"); 
		
		Offer offer = offerRepo.findBySourceName(sourceName);
		double offerAmount = offer.getCurrentOffer();
		
		
		Giftcard card = GiftcardUtil.createNewGiftcard(email, cardNumber, cardPin, proclaimedBalance, sourceName, offerAmount);
		giftcardRepo.save(card);
		
		return "Card uploaded, will verify within the next 12 hours"; 
	}
	
	public String resubmitCard(String authHeader, String cardNumber) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		
		Giftcard card = giftcardRepo.findByNumber(cardNumber);
		if(card == null || !card.getEmail().equals(email))
			throw new ServiceError("This card does not exist");
		
		if(!card.getCurrentStatus().equals(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM)) 
			throw new ServiceError("Can not resubmit this card");
		
		if(card.getResubmitCount() > 5)
			throw new ServiceError("Can no longer resubmit this card"); 
		
		card.setResubmitCount(card.getResubmitCount()+1);
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING);
		card.setLastUpdate(LocalDate.now());
		
		if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_BALANCE))
			card.setProclaimedBalance(card.getVerifiedBalance());
		
		card.setPayoutBalance(card.getProclaimedBalance()*card.getPayoutRate());
		
		
		if(card.getErrorType().equals(VersionController.GIFTCARD_ERROR_VERIFY_DUPLICATE)) {
			return this.deleteCard(authHeader, card.getNumber());
		}
		
		giftcardRepo.save(card);
		return "Card resubmited";
	}
	
	public String deleteCard(String authHeader, String cardNumber) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		
		Giftcard card = giftcardRepo.findByNumber(cardNumber);
		if(card == null || !card.getEmail().equals(email))
			throw new ServiceError("This card does not exist");
		
		if(!card.getCurrentStatus().equals(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM)) 
			throw new ServiceError("Can not delete this card");
		
		giftcardRepo.delete(card);
		return "Card deleted";
	}
	
	public List<GiftcardResult> generalSearch(String authHeader) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		List<Giftcard> cards = giftcardRepo.findByEmail(email);
		cards = GiftcardUtil.sortCards(cards);
		return GiftcardUtil.convertToResults(cards); 
	}
	
	public List<GiftcardResult> search(String authHeader, String searchQuery) throws ServiceError{
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		
		if(!searchQuery.startsWith("CUSTOM_") || searchQuery.split("CUSTOM_").length != 2)
			throw new ServiceError("Invalid search"); 
		
		String query = searchQuery.split("CUSTOM_")[1].trim();
		List<Giftcard> results = new ArrayList<>();
		
		if(query.equals(VersionController.GIFTCARD_SEARCH_VERIFYING)) {
			results = giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_VERIFYING, email);
		} else if(query.equals(VersionController.GIFTCARD_SEARCH_PROBLEMS)) {
			results = giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_SPENDING_PROMBLEM, email);
			results.addAll(giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM, email));
		} else if(query.equals(VersionController.GIFTCARD_SEARCH_SPENDING)) {
			results = giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_SPENDING, email);
		} else if(query.equals(VersionController.GIFTCARD_SEARCH_PAYING)) {
			results = giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_PAYING, email);
		} else if(query.equals(VersionController.GIFTCARD_SEARCH_PAID)) {
			results = giftcardRepo.findByCurrentStatusAndEmail(VersionController.GIFTCARD_STATUS_PAID, email);
		} else if(query.equals(VersionController.GIFTCARD_SEARCH_SETTLED)) {
			List<SettledGiftcard> cards = settledGiftcardRepo.findByEmail(email);
			return GiftcardUtil.convertToResultsSettled(cards); 
		}
		return GiftcardUtil.convertToResults(results);
	}
	
	public GiftcardDisplay getGiftcard(String authHeader, String cardNumber) throws ServiceError {
		String email = GeneralUtil.getEmailFromTokenFromHeader(authHeader);
		if(email.equals(""))
			throw new ServiceError("Invalid token"); 
		
		Giftcard card = giftcardRepo.findByEmailAndNumber(email, cardNumber);
		if(card != null) 
			return GiftcardUtil.convertToDisplay(card);
		SettledGiftcard settledCard = settledGiftcardRepo.findByEmailAndNumber(email, cardNumber);
		if(settledCard != null)
			return GiftcardUtil.convertSettledToDisplay(settledCard);
		throw new ServiceError("No card exists");
	}
	
	
}
