package rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.model.payments.frontend.PaymentResponse;
import data.model.payments.frontend.PaymentSearchResult;
import rest.service.PaymentService;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@CrossOrigin
@RequestMapping("/api/payments")
public class PaymentsController {
	
	@Autowired
	private PaymentService service;
	
	@GetMapping("/get/{id}")
	public ResponseEntity<Object> getPayment(@RequestHeader(value="Authorization") String authHeader, @PathVariable String id) {
		try {
			PaymentResponse response = service.getPayment(authHeader, id);
			return ResponseEntity.ok(response); 
		} catch(ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}

	@GetMapping("/all") 
	public ResponseEntity<Object> getAll(@RequestHeader(value="Authorization") String authHeader) {
		try {
			List<PaymentSearchResult> results = service.getResults(authHeader);
			return ResponseEntity.ok(results);
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
}
