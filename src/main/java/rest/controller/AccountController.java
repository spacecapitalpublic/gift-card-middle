package rest.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.model.accounts.frontend.ForgotChangePassword;
import data.model.accounts.frontend.ForgotPasswordRequest;
import data.model.accounts.frontend.LoginResponseLocal;
import data.model.accounts.frontend.SignInRequest;
import data.model.accounts.frontend.SignUpRequest;
import rest.service.AccountService;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@CrossOrigin
@RequestMapping("/accounts")
public class AccountController {
	
	@Autowired
	private AccountService accountService;

	@PostMapping("/sign-up")
	public ResponseEntity<BasicResponse> signUp(@RequestBody SignUpRequest request) {
		try {
			accountService.signUp(request);
			return ResponseEntity.ok(new BasicResponse("Account registered"));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/sign-in")
	public ResponseEntity<Object> signIn(@RequestBody SignInRequest request, HttpServletRequest httpRequest, @RequestHeader HttpHeaders headers) {
		try {
			String ip = headers.get("x-real-ip").toString(); 
			if(ip==null)
				ip = "";
			LoginResponseLocal response = accountService.signIn(request, ip);
			ResponseCookie cookie = ResponseCookie.from("auth", response.getCookieValue()).httpOnly(false).maxAge(864000).path("/").build();
			return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(response.getResponse());
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@GetMapping("/verify/{code}")
	public ResponseEntity<BasicResponse> verify(@PathVariable String code) {
		try {
			accountService.verifyAccount(code);
			return ResponseEntity.ok(new BasicResponse("Account verified, please log in"));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/forgotpassword")
	public ResponseEntity<BasicResponse> forgotPassword(@RequestBody ForgotPasswordRequest request) {
		try {
			String response = accountService.forgotPassword(request);
			return ResponseEntity.ok(new BasicResponse(response));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/changepassword")
	public ResponseEntity<BasicResponse> changePassword(@RequestBody ForgotChangePassword request) {
		try {
			String response = accountService.changePassword(request);
			return ResponseEntity.ok(new BasicResponse(response));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/token")
	public ResponseEntity<BasicResponse> checkToken(@RequestHeader(value="Authorization") String authHeader, @CookieValue("auth") String authCookie) {
		try {
			accountService.isTokenGood(authHeader, authCookie);
			return ResponseEntity.ok(new BasicResponse("Good")); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse("Invalid or expired")); 
		}
	}
	
	
}
