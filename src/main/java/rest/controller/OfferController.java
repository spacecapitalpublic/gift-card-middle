package rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.model.offers.frontend.OfferDisplay;
import rest.service.OfferService;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@CrossOrigin
@RequestMapping("/api/offers")
public class OfferController {
	
	@Autowired
	private OfferService service;
	
	@GetMapping("/all")
	public ResponseEntity<Object> getAllOffers() {
		return ResponseEntity.ok(service.getAllOffers());
	}
	
	@GetMapping("/get/{sourceName}")
	public ResponseEntity<Object> getOffer(@PathVariable String sourceName) {
		try {
			OfferDisplay display = service.getOffer(sourceName);
			return ResponseEntity.ok(display);
		} catch(ServiceError error) {
			return ResponseEntity.badRequest().body(new BasicResponse(error.getResponseError()));
		}
	}
}
