package rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import data.model.giftcards.frontend.GiftcardDisplay;
import data.model.giftcards.frontend.GiftcardResult;
import data.model.giftcards.frontend.GiftcardUpload;
import rest.service.GiftcardService;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@CrossOrigin
@RequestMapping("/api/giftcards")
public class GiftcardController {

	@Autowired
	private GiftcardService giftcardService;
	
	@PostMapping("/upload")
	public ResponseEntity<BasicResponse> uploadCard(@RequestHeader(value="Authorization") String authHeader, @RequestBody GiftcardUpload upload) {
		try {
			String response = giftcardService.uploadGiftcard(authHeader, upload);
			return ResponseEntity.ok(new BasicResponse(response)); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@GetMapping("/generalsearch")
	public ResponseEntity<Object> generalSearch(@RequestHeader(value="Authorization") String authHeader) {
		try {
			List<GiftcardResult>  response = giftcardService.generalSearch(authHeader);
			return ResponseEntity.ok(response); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@GetMapping("/search")
	public ResponseEntity<Object> searchCards(@RequestHeader(value="Authorization") String authHeader, @RequestParam String search) {
		try {
			List<GiftcardResult>  response = giftcardService.search(authHeader, search);
			return ResponseEntity.ok(response); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}

	@GetMapping("/get/{cardNumber}")
	public ResponseEntity<Object> getCard(@RequestHeader(value="Authorization") String authHeader, @PathVariable String cardNumber) {
		try {
			GiftcardDisplay display = this.giftcardService.getGiftcard(authHeader, cardNumber);
			return ResponseEntity.ok(display);
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/resubmit/{cardNumber}")
	public ResponseEntity<Object> resubmitCard(@RequestHeader(value="Authorization") String authHeader, @PathVariable String cardNumber) {
		try {
			String response = this.giftcardService.resubmitCard(authHeader, cardNumber);
			return ResponseEntity.ok(new BasicResponse(response));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/delete/{cardNumber}")
	public ResponseEntity<Object> deleteCard(@RequestHeader(value="Authorization") String authHeader, @PathVariable String cardNumber) {
		try {
			String response = this.giftcardService.deleteCard(authHeader, cardNumber);
			return ResponseEntity.ok(new BasicResponse(response));
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
}
