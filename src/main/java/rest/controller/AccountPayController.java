package rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import data.model.accounts.frontend.AccountPayementResponse;
import data.model.accounts.frontend.AccountSummary;
import data.model.accounts.frontend.ChangePasswordRequest;
import data.model.accounts.frontend.PaymentMethodsUpdate;
import rest.service.AccountService;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@CrossOrigin
@RequestMapping("/api/account")
public class AccountPayController {
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/summary")
	public ResponseEntity<Object> getAccountSummary(@RequestHeader(value="Authorization") String authHeader) {	
		try {
			AccountSummary summary = accountService.getAccountSummary(authHeader);
			return ResponseEntity.ok(summary);
		} catch(ServiceError error) {
			return ResponseEntity.badRequest().body(new BasicResponse(error.getResponseError()));
		}
	}
	@GetMapping("/paymentmethods")
	public ResponseEntity<Object> getAccountPayements(@RequestHeader(value="Authorization") String authHeader) {
		try {
			AccountPayementResponse payements = this.accountService.getAccountPayements(authHeader);
			return ResponseEntity.ok(payements);
		} catch(ServiceError error) {
			return ResponseEntity.badRequest().body(new BasicResponse(error.getResponseError()));
		}
	}
	
	@PostMapping("/paymentmethods/update")
	public ResponseEntity<BasicResponse> updatePaymentMethods(@RequestHeader(value="Authorization") String authHeader,
			@RequestBody PaymentMethodsUpdate request) {
		try {
			String response = this.accountService.updatePaymentMethods(authHeader, request);
			return ResponseEntity.ok(new BasicResponse(response)); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("/changepassword")
	public ResponseEntity<BasicResponse> changePassword(@RequestHeader(value="Authorization")  String authHeader, @RequestBody ChangePasswordRequest request) {
		try {
			String response = accountService.changePassword(authHeader, request);
			return ResponseEntity.ok(new BasicResponse(response)); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	
}
